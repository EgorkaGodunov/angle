CXX=g++
CFLANG=-Wall --std=c++11

ifeq ($(OS), Windows_NT)
	RM=del
else
	RM=rm
endif
all:
ifeq ($(OS), Windows_NT)
	$(CXX) $(CFLAGS) main.cpp -o main
	$(CXX) $(CFLAGS) second.cpp -o second
else
	g++ -o main main.cpp
	g++ -o second second.cpp
endif


clean:
	$(RM) *.o *.gch *.txt *.exe
main:
	$(CXX) $(CFLAGS) main.cpp -o main
second:
	$(CXX) $(CFLAGS) second.cpp -o second
run: all
ifeq ($(OS), Windows_NT)
	.\main.exe > out.txt
	.\second.exe > angles.txt
else
	./main > out.txt
	./second > angles.txt
endif
